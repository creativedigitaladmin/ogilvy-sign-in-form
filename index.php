<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src='https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js'></script>  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="js/index.js"></script>

<body>

        
 <div class="section"> 
      <div for="tbEmail" class="lbl-tb">
            <?php 
                session_start();
                if(!empty($_SESSION["message"])){
                  echo $_SESSION["message"];
                  session_unset();
                }
             ?>
          </div>
          <div class="logo">  <img src="img/signature.png"></div>
            
          <form method="POST" autocomplete="off" action="registration.php" name="registration">
           <div class="wrapper">
                 <label for="tbEmail" class="lbl-tb"> Name </label>
                 <br />
                 <input id="name" type="text" class="frm-ctrl tb" name="name" autocomplete="on" />
                
           </div>

           <div class="wrapper">
                <label for="tbPwd1" class="lbl-tb"><i class="fa fa-unlock fa-fw"></i> Surname</label>
                <br />
                <input id="surname" type="text" class="frm-ctrl tb" name="surname" autocomplete="on"/>
               
            </div>

            <div class="wrapper">
               <label for="tbPwd2" class="lbl-tb"> ID number</label>
               <br />
               <input id="id_number" type="numbers" class="frm-ctrl tb" name="id_number"/>
            </div>

            <div class="wrapper">
                <label for="tbPwd2" class="lbl-tb">Company</label>
                <br />
                <input id="company" type="text" class="frm-ctrl tb" name="company" />
            </div>

            <div class="wrapper">
                <label for="tbPwd2" class="lbl-tb">Contact Number</label>
                <br />
                <input id="contact_number" type="numbers" class="frm-ctrl tb" name="contact_number" />
            </div>

            <div class="wrapper">
                <label for="tbPwd2" class="lbl-tb">Person visiting </label>
                <br/>
                <input id="person_visiting" type="text" class="frm-ctrl tb" name="person_visiting" data-email="" data-name=""   />
              <input id="staff_email" type="hidden" class="staff-members" name="staff_email" value=""  /> 
                <div class="staff-members"> 
                  
                </div>  
            </div>
         <div class="wrapper submit">
             <b>  <input type="submit" class="frm-ctrl btn" id="myBtn" />
         </div>
       </form>

    </div>   


    <script>
  $(document).ready(function(){

  // Initialize form validation on the registration form.
  $("form[name='registration']").validate({
    // Specify validation rules
    rules: {

      // Validation rules are defined
      name: "required",
      surname: "required",
      company: "required",
      person_visiting: "required",
      id_number: {
        required: true,
        // Specify that numbers should be validated
        // by the built-in "email" rule
        number: true,
        minlength: 13,
        maxlength: 13
      },
       contact_number: {
         number: true,
        required: true,
        minlength: 10
      }

    },
    // Specify validation error messages
    messages: {
      name: "Please fill out the field ",
      surname: "Please fill out the field",
      contact_number: {
        required: "Please provide a contact number",
             },
      contacts_number: "Please enter correct contact number"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();

      alert("WELCOME");
    }
  });
});




</script>






  </body>
</html>


                    
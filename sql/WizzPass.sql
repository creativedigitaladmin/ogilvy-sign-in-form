-- MySQL dump 10.13  Distrib 5.6.31, for Win32 (AMD64)
--
-- Host: localhost    Database: WizzPass
-- ------------------------------------------------------
-- Server version	5.6.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `surname` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mobile` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'Will','Roos','will.roos@ogilvy.co.za',810126488,'0000-00-00 00:00:00'),(2,'Wally','Where','where@is.wally',0,NULL),(3,'pumi','ngundze','ngundze@gmail.com',834900290,'2017-10-09 00:00:00'),(4,'pre ','namie','pre@gmail.com',834900290,'2017-10-09 00:00:00'),(7,'pham ','phumzie ','pmz@gmail.com',834900290,NULL),(8,'phumzile','ngu','pza@gmail.com',2565653,NULL),(9,'pham ','phumzie ','pmz@gmail.com',834900290,NULL),(10,'phumzile','ngu','pza@gmail.com',2565653,NULL),(11,'lulama','ngundze','pula@gmail.com',14553126,NULL),(12,'loyiso','ngwebo','ngqwebol@gmail.com',24565465,NULL);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `surname` varchar(40) CHARACTER SET utf8 NOT NULL,
  `id_number` varchar(40) CHARACTER SET utf8 NOT NULL,
  `company` varchar(40) CHARACTER SET utf8 NOT NULL,
  `contact_number` varchar(40) CHARACTER SET utf8 NOT NULL,
  `person_visiting` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_visiting` (`person_visiting`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'cvbncv','bncvbn','cbvn','cvbnbv','cvbncvb','ncvbnc',NULL),(2,'','','','','','',NULL),(3,'Will','Roos','854758655958','Ogilvy','08102488566','Myself',NULL),(4,'pumla','ngundze','9109180602088','ogilvy','0834900290','pumla',NULL),(5,'cvgxcgf','cfvgcvcv','cvhbcvbh','cvhbcvbhcvb','cvbhcvbcv','loyiso.ngqwebo@gmail.com',NULL),(6,'ghghfg','ghfghfgh','213123123123123','dfgdfgf','122123544','loyiso.ngqwebo@gmail.com',NULL),(7,'dfza','\\h','zdbhdg','dhfzh','zdhdrzf','hf',NULL),(8,'dsfasdas','sfdasdasd','sdasdasd','sdasdasd','sdasdasdsa','loyiso.ngqwebo@gmail.com',NULL),(9,'ydrtdrft','fgdfgdfg','124255465212454','fxgdsfgdfs','1234567890','loyiso.ngqwebo@gmail.com',NULL),(10,'dfdsfsd','dfsdfds','dfsdfsdf','dsfsdfsdf','dfsdfsdfds','loyiso.ngqwebo@gmail.com',NULL),(11,'ghghgfh','hjghjgh','hhjghjgh','hjghjghjghj','hjughjghjghj','ngqwebol@gmail.com',NULL),(12,'dfgxdg','fgdfgd','fgdfgdf','fgdfgdf','dfgdfgdf','ngqwebol@gmail.com',NULL),(13,'sdasdas','sdasdas','sdasdas','sdasdasd','sdasdas','kkkkk',NULL),(14,'sadas','dsdas','dsdasd','sdasd','asdasd','sdasdasd',NULL),(15,'dsfsdf','dfsdf','dsfsdf','dfsdf','dfsdfsdf','ngqwebol@gmail.com',NULL),(16,'dsfdsfsdf','dfsdfsdf','dfsdfsdfdsf','dfsdfsdf','dfsdfdsfds','ngqwebol@gmail.com',NULL),(17,'sdasdasd','sdasdas','sdasdasd','sdasdasd','sdasdasd','ngqwebol@gmail.com',NULL),(18,'dsfsdfdf','dfsdfsdf','dfsdfsdf','dfsdfsdf','dfsdfsdf','pumlangundze@gmail.com',NULL),(19,'sdasd','sdasd','sdasd','sdasd','sdasd','pumlangundze@gmail.com',NULL),(20,'s','','','','','',NULL),(21,'ddddd','dddd','5456544568','ogilvy','57785666','loyiso ngwebo',NULL),(22,'sdasds','sdas','dsdasd','dasdasds','sdasd','sdasdasd',NULL),(23,'dfsd','dfsdf','dsfsdf','dfsdf','dfsdf','dfsdfsd',NULL),(24,'name','ngundz','5454563566','gdijudijuij','1234567891','loyiso ngwebo',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-11 12:14:06
